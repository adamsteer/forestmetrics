# Forestmetrics

Adam Steer
Spatialised: https://spatialised.net

### Important: This code is being refactored and may not work

This repository contains Python code for computing various forest indices from airborne lidar, defined by the Australian Terrestrial Ecosystems Researcn Network (TERN), https://tern.org.au . Calculations are based on a 2018 [Airborne Products Manual](./references/TERN_airborne_ProductSpecification.pdf), reproduced in this repository until a DOI can be found.

The [Point Data Abstraction Library](https://pdal.io) delivers core functionality for point cloud processing in this toolkit. Other critical software pieces are [Shapely](https://shapely.readthedocs.io/en/stable/manual.html) and [GDAL](https://gdal.org).

Examples can be found in [Jupyter notebooks](./notebooks), stepping through the process of working from lidar tiles to output products as GeoTIFFs. These assume a Python virtual environment. In general I've used Conda or Mamba package managers to set up and run these code examples.

## What does it do?

The products computed are:

*Vegetation cover fraction*  (Nfirst - Nsingle) / Nfirst

